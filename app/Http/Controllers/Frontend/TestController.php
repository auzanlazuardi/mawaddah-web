<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Test;

class TestController extends Controller
{
    public function create(Request $request)
    {
    	$data['nama'] 	= $request->nama;
    	$data['email'] 	= $request->email;

    	try{

    		Test::create($data);	
    	}
    	catch(RequestException $e){
    		$e->getMessage();
    	}
    	

    	return response()->json([
    		'responseCode'		=> '01',
    		'responseMessage'	=> 'Data berhasil ditambahkan'
    	]);
    }

    public function update(Request $request)
    {
    	$email = $request->email;
    	$id	= $request->id;

    	$checkEmail = Test::where('email', $email)->first();

    	if((count($checkEmail) == 1 and $checkEmail['id'] == $id) or (count($checkEmail) == 0))
    	{

    		Test::where('id', $id)->update([
    			'email' => $email
    		]);

    		$updated = Test::where('id', $id)->first();

	    	return response()->json([
	    		'responseCode'		=> '0',
	    		'responseMessage'	=> 'Data berhasil ditambah',
	    		'data'				=> $updated
    		]);
    	}
    	else
    	{
    		return response()->json([
	    		'responseCode'		=> '0',
	    		'responseMessage'	=> 'Email telah digunakan oleh user lain',
    		]);
    	}

    	
    }
}
