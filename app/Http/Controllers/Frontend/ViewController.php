<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUs;
use App\Mail\ContactUsConfirmation;
use GuzzleHttp\Client;

class ViewController extends Controller
{
    public function index()
    {
    	$data['title'] = 'Beranda';

        $client = new Client();
        $response = $client->request('GET', 'http://localhost:8080/api/getGallery');
        $body = $response->getBody();

        $data['obj'] = json_decode($body);
    	return view('frontend.index', $data);
    }

    public function galeri()
    {
    	$data['title'] = 'Galeri';

    	return view('frontend.galeri.index', $data);
    }

    public function tentangkami()
    {
        $data['title'] = 'Tentang Kami';

        return view('frontend.informasi.tentangkami', $data);
    }

    public function persyaratan()
    {
        $data['title'] = 'Persyaratan Umroh';

        return view('frontend.informasi.persyaratan', $data);
    }

    public function kontak()
    {
        $data['title'] = 'Hubungi Kami';

        return view('frontend.hubungi.kontak', $data);
    }

    public function sendEmail(Request $request)
    {
        $data = [
            'name'      =>  $request->name,
            'email'     =>  $request->email,
            'message'   =>  $request->message,
            'travel'    =>  'Mawaddah Tour'
        ];

        try{
            Mail::to($data['email'])->send(new ContactUsConfirmation($data));
            Mail::to('mawaddahtourhelp@gmail.com')->send(new ContactUs($data));

        }catch(RequestException $e){
            $e->getMessage();
        }
        

        return response()->json([
            'responseCode'      => '01',
            'responseMessage'   => 'Berhasil mengirim email!'
        ]);

    }
}
