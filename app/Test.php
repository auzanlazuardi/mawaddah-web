<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'test';
    public $timestamps = false;
    protected $fillable = ['nama', 'email'];
}
