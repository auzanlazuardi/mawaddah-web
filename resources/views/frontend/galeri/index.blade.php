@extends('frontend.master.layout')

@section('content')
<section class="page-header">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-md-8 text-left">
								<span class="tob-sub-title text-color-primary d-block">Galeri</span>
								<h1 class="font-weight-bold">Galeri</h1>
								<p class="lead">Kumpulan dokumentasi foto bersama MawaddahTour</p>
							</div>
							<div class="col-md-4">
								<ul class="breadcrumb justify-content-start justify-content-md-end mb-0">
									<li><a href="{{ route('beranda') }}">Beranda</a></li>
									<li class="active">Galeri</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<ul id="portfolioPaginationFilter" class="nav sort-source mb-4" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
						<li class="nav-item" data-option-value="*"><a class="nav-link active" href="#">Dokumentasi galeri</a></li>
					</ul>

					<div class="sort-destination-loader sort-destination-loader-showing">
						<div id="portfolioPaginationWrapper" class="portfolio-list portfolio-list-style-2 sort-destination mb-4" data-sort-id="portfolio" data-items-per-page="4">

							<div class="col-md-4 col-lg-3 p-0 isotope-item brands mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/generic/project-1.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">EZ Card</h2>
									<span class="text-uppercase">brands</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item design mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/photos/project-2.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Fashion Model</h2>
									<span class="text-uppercase">design</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item web mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/generic/project-3.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Style Bag</h2>
									<span class="text-uppercase">web</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item web mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/photos/project-3.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Shoes EZ</h2>
									<span class="text-uppercase">web</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item ads mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/generic/project-4.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Sport Watch</h2>
									<span class="text-uppercase">ads</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item design mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/photos/project-6.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Tattoo Show</h2>
									<span class="text-uppercase">design</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item ads mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/photos/project-12.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Mobile Tech</h2>
									<span class="text-uppercase">ads</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item design mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/generic/project-6.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">EZ Book</h2>
									<span class="text-uppercase">design</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item web mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/generic/project-7.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Laptop Concept</h2>
									<span class="text-uppercase">web</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item brands mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/photos/project-5.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Pink Ad</h2>
									<span class="text-uppercase">brands</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item ads mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/generic/project-8.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Shirt Box</h2>
									<span class="text-uppercase">ads</span>
								</div>
							</div>

							<div class="col-md-4 col-lg-3 p-0 isotope-item web mb-3">
								<div class="portfolio-item hover-effect-3d text-center">
									<a href="frontend/img/projects/photos/project-3-square.jpg" class="galeri">
										<span class="image-frame image-frame-style-1 image-frame-effect-1 mb-3">
											<span class="image-frame-wrapper">
												<img src="frontend/img/projects/photos/project-7.jpg" class="img-fluid" alt="">
												<span class="image-frame-inner-border"></span>
												<span class="image-frame-action">
													<span class="image-frame-action-icon">
														<i class="lnr lnr-link text-color-light"></i>
													</span>
												</span>
											</span>
										</span>
									</a>
									<h2 class="font-weight-bold text-4 mb-0">Montaing Woman</h2>
									<span class="text-uppercase">web</span>
								</div>
							</div>
						</div>
						<hr class="mb-4">
						<div class="row align-items-center justify-content-between">
							<div class="col-auto mb-3 mb-sm-0">
								<span></span>
							</div>
							<div class="col-auto">
								<div id="portfolioPagination" class="float-right"></div>
							</div>
						</div>
					</div>


				</div>
@endsection

@section('js')
<script src="{{ asset('frontend/vendor/bootpag/bootpag.js')}}"></script>
<script src="{{ asset('frontend/js/examples/examples.portfolio.js')}}"></script>
<script type="text/javascript">
	window.onload = function () {
		halkaBox.run("galeri");
	}
</script>
@endsection