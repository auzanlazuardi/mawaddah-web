@extends('frontend.master.layout')

@section('content')
<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 px-0">
							<section class="section bg-primary">
								<div class="row justify-content-end m-0">
									<div class="col-half-section pr-md-5">
										<div class="icon-box icon-box-style-4 align-items-center mb-4 appear-animation" data-appear-animation="fadeInUpShorter">
											<div class="icon-box-icon bg-dark-4">
												<i class="lnr lnr-apartment text-color-light"></i>
											</div>
											<div class="icon-box-info">
												<div class="icon-box-info-title">
													<h2 class="font-weight-bold text-color-light text-4 mb-0">Address</h2>
												</div>
												<p class="text-color-light text-2 opacity-8 mb-0">1234 Street Name, City Name, USA</p>
											</div>
										</div>
										<div class="icon-box icon-box-style-4 align-items-center mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
											<div class="icon-box-icon bg-dark-4">
												<i class="lnr lnr-envelope text-color-light"></i>
											</div>
											<div class="icon-box-info">
												<div class="icon-box-info-title">
													<h2 class="font-weight-bold text-color-light text-4 mb-0">Support E-mail Address</h2>
												</div>
												<a href="mailto:example@domain.com" class="link-color-light-2 text-2 opacity-8">example@domain.com</a>
											</div>
										</div>
										<div class="icon-box icon-box-style-4 align-items-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
											<div class="icon-box-icon bg-dark-4">
												<i class="lnr lnr-phone-handset text-color-light"></i>
											</div>
											<div class="icon-box-info">
												<div class="icon-box-info-title">
													<h2 class="font-weight-bold link-color-light-2 text-4 mb-0">Sales Phone Number</h2>
												</div>
												<span class="d-block text-color-light">
													<a href="tel:+1234567890" class="link-color-light-2 text-2 opacity-8">(123) 456-7890</a> - <a href="tel:+1234567890" class="link-color-light-2 text-2 opacity-8">(123) 456-7890</a>
												</span>
											</div>
										</div>
									</div>
								</div>
							</section>
							<section class="section bg-primary-4">
								<div class="row justify-content-end m-0">
									<div class="col-half-section pr-md-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">
										<div class="row">
											<div class="col">
												<span class="top-sub-title text-color-light opacity-6">Silahkan masukkan email anda yang valid agar kami dapat membalas pesan anda sesegera mungkin</span>
												<h2 class="text-color-light font-weight-bold mb-4">Hubungi kami</h2>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<form class="contact-form form-style-3 form-errors-light" action="{{ route('sendEmail') }}" method="POST">
													{{ csrf_field() }}
													<div class="contact-form-success alert alert-success d-none">
														<strong>Success!</strong> Your message has been sent to us.
													</div>
													<div class="contact-form-error alert alert-danger d-none">
														<strong>Error!</strong> There was an error sending your message.
														<span class="mail-error-message d-block"></span>
													</div>
													<div class="form-row">
														<div class="form-group col-lg-6">
															<input type="text" value="" data-msg-required="Silahkan masukkan nama anda" maxlength="100" class="form-control" name="name" id="name" placeholder="Nama" required>
														</div>
														<div class="form-group col-lg-6">
															<input type="email" value="" data-msg-required="Silahkan masukkan email anda" data-msg-email="Silahkan masukkan email yang benar" maxlength="100" class="form-control" name="email" id="email" placeholder="E-mail" required>
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col">
															<textarea maxlength="5000" data-msg-required="Silahkan masukkan pesan anda" rows="5" class="form-control" name="message" id="message" placeholder="Pesan" required></textarea>
														</div>
													</div>
													<div class="form-row mt-2">
														<div class="col">
															<input type="submit" value="KIRIM" class="btn btn-dark btn-rounded btn-4 font-weight-semibold text-0" data-loading-text="Loading...">
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<div class="col-md-6 px-0">
							<!-- Go to the bottom of the page to change settings and map location. -->
							<div id="googlemaps" class="google-map min-height-370 h-100"></div>
						</div>
					</div>
				</div>
@endsection

@section('js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhpYHdYRY2U6V_VfyyNtkPHhywLjDkhfg"></script>
		<script>

			/*
			Map Settings

				Find the Latitude and Longitude of your address:
					- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
					- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

			*/
			(function( $ ) {

				'use strict';

				// Map Markers
				var mapMarkers = [{
					address: "New York, NY 10017",
					html: "<strong>New York Office</strong><br>New York, NY 10017",
					icon: {
						image: "img/pin-blue.png",
						iconsize: [26, 46],
						iconanchor: [12, 46]
					}
				}];

				// Map Initial Location
				var initLatitude = 40.75198;
				var initLongitude = -73.96978;

				// Map Extended Settings
				var mapSettings = {
					controls: {
						panControl: true,
						zoomControl: true,
						mapTypeControl: true,
						scaleControl: true,
						streetViewControl: true,
						overviewMapControl: true
					},
					scrollwheel: false,
					markers: mapMarkers,
					latitude: initLatitude,
					longitude: initLongitude,
					zoom: 4
				};

				var map = $('#googlemaps').gMap(mapSettings);

				var mapRef = $('#googlemaps').data('gMap.reference');

				// Styles from https://snazzymaps.com/
				var styles = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];

				var styledMap = new google.maps.StyledMapType(styles, {
					name: 'Styled Map'
				});

				mapRef.mapTypes.set('map_style', styledMap);
				mapRef.setMapTypeId('map_style');

				// Redraw Map On Resize
				$(window).afterResize(function(){
					$('#googlemaps').gMap(mapSettings);
					var mapRef = $('#googlemaps').data('gMap.reference');

					mapRef.mapTypes.set('map_style', styledMap);
					mapRef.setMapTypeId('map_style');
				});

			}).apply( this, [ jQuery ]);

		</script>
@endsection