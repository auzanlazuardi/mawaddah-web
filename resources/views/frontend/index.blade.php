@extends('frontend.master.layout')

@section('content')

				<div class="slider-container slider-container-full-height rev_slider_wrapper">
					<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'sliderLayout': 'fullscreen', 'gridwidth': [1140,960,720,540], 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,576], 'navigation' : {'arrows': { 'enable': true, 'hide_under': 767, 'style': 'slider-arrows-style-1' }, 'bullets': {'enable': false, 'style': 'bullets-style-1', 'h_align': 'right', 'v_align': 'bottom', 'space': 7, 'v_offset': 35, 'h_offset': 35}}}">
						<ul>
							<li class="slide-overlay slide-overlay-primary slide-overlay-level-9" data-transition="fade">
								<img src="frontend/img/slides/multi-purpose/slide-5-1.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<div class="tp-caption bg-dark-5"
									data-x="['460','360','260','160']"
									data-y="center" data-voffset="['110','110','110','110']"
									data-start="1300"
									data-width="1500"
									data-height="['550','550','550','550']"
									data-basealign="grid"
									data-transform_in="y:[-10%];opacity:0;s:500;"
									data-transform_idle="skX:-19deg;opacity:0.5;"
									data-transform_out="y:[-10%];opacity:0;s:500;"></div>

								<div class="tp-caption bg-dark-5"
									data-x="['560','460','360','260']"
									data-y="center" data-voffset="['30','30','30','30']"
									data-start="1000"
									data-width="1500"
									data-height="['550','550','550','550']"
									data-basealign="grid"
									data-transform_in="y:[-10%];opacity:0;s:500;"
									data-transform_idle="skX:-19deg;opacity:1;"
									data-transform_out="y:[-10%];opacity:0;s:500;"></div>

								<div class="tp-caption text-color-light font-primary font-weight-light"
									data-x="center" data-hoffset="['-423','-313','-240','-170']"
									data-y="center" data-voffset="['-80','-80','-65','-55']"
									data-start="1600"
									data-fontsize="['16','16','14','13']"
									data-transform_in="y:[-50%];opacity:0;s:500;"
									data-transform_out="y:[-50%];opacity:0;s:500;">WHAT WE DO?</div>

								<h1 class="tp-caption text-color-light font-weight-bold"
									data-x="center"
									data-y="center" data-voffset="['-40','-40','-30','-25']"
									data-start="1900"
									data-fontsize="['65','50','40','30']"
									data-lineheight="['70','55','45','35']"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="y:[100%];s:500;"
									data-mask_in="x:0px;y:0px;">We build awesome websites</h1>

								<div class="tp-caption text-color-light font-primary font-weight-bold"
									data-x="center" data-hoffset="['105','80','60','43']"
									data-y="center" data-voffset="['40','20','20','15']"
									data-start="2200"
									data-fontsize="['65','50','40','30']"
									data-lineheight="['70','55','45','35']"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="y:[100%];s:500;"
									data-mask_in="x:0px;y:0px;">and provide solutions</div>

								<div class="tp-caption text-color-primary font-quaternary font-weight-light"
									data-x="center" data-hoffset="['315','240','185','135']"
									data-y="center" data-voffset="['130','95','85','75']"
									data-start="2500"
									data-fontsize="['50','40','35','25']"
									data-transform_in="opacity:0;s:500;"
									data-transform_out="y:[50%];opacity:0;s:500;">Get Started!</div>

								<a class="tp-caption text-color-light"
									href="#aboutus"
									data-hash
									data-hash-offset="60"
									data-x="center"
									data-y="bottom" data-voffset="['40','40','40','40']"
									data-start="2800"
									data-basealign="slide"
									data-fontsize="['45','45','45','45']"
									data-transform_in="y:[-50%];opacity:0;s:500;"
									data-transform_out="y:[-50%];opacity:0;s:500;"><i class="fas fa-angle-down"></i></a>
								
							</li>
							<li class="slide-overlay slide-overlay-level-6" data-transition="fade">
								<img src="frontend/img/slides/multi-purpose/slide-5-2.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									data-kenburns="on" 
									data-duration="20000" 
									data-ease="Linear.easeNone" 
									data-scalestart="110" 
									data-scaleend="100" 
									data-offsetstart="250 100" 
									data-offsetend="-250 -150" 
									class="rev-slidebg">

								<div class="tp-caption text-color-light font-primary font-weight-light"
									data-x="center" data-hoffset="['-455','-330','-260','-185']"
									data-y="center" data-voffset="['-90','-80','-70','-60']"
									data-start="1600"
									data-fontsize="['16','16','14','13']"
									data-transform_in="y:[-50%];opacity:0;s:500;"
									data-transform_out="y:[-50%];opacity:0;s:500;">WE CAN HELP YOU</div>

								<div class="tp-caption text-color-light font-primary font-weight-bold"
									data-x="center"
									data-y="center" data-voffset="['-40','-40','-30','-25']"
									data-start="1900"
									data-fontsize="['65','50','40','30']"
									data-lineheight="['70','55','45','35']"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="y:[100%];s:500;"
									data-mask_in="x:0px;y:0px;">Contact us now and learn more</div>

								<div class="tp-caption text-color-light font-primary font-weight-bold"
									data-x="center" data-hoffset="['210','165','130','100']"
									data-y="center" data-voffset="['40','20','20','15']"
									data-start="2200"
									data-fontsize="['65','50','40','30']"
									data-lineheight="['70','55','45','35']"
									data-transform_in="y:[100%];s:500;"
									data-transform_out="y:[100%];s:500;"
									data-mask_in="x:0px;y:0px;">about our services.</div>

								<div class="tp-caption text-color-primary font-quaternary font-weight-light"
									data-x="center" data-hoffset="['320','250','185','135']"
									data-y="center" data-voffset="['130','95','95','75']"
									data-start="2500"
									data-fontsize="['50','40','35','25']"
									data-transform_in="opacity:0;s:500;"
									data-transform_out="y:[50%];opacity:0;s:500;">GET IN TOUCH!</div>

								<a class="tp-caption text-color-light"
									href="#aboutus"
									data-hash
									data-hash-offset="60"
									data-x="center"
									data-y="bottom" data-voffset="['40','40','40','40']"
									data-start="2800"
									data-basealign="slide"
									data-fontsize="['45','45','45','45']"
									data-transform_in="y:[-50%];opacity:0;s:500;"
									data-transform_out="y:[-50%];opacity:0;s:500;"><i class="fas fa-angle-down"></i></a>
								
							</li>
						</ul>
					</div>
				</div>
				<section id="aboutus" class="section pb-lg-0">
					<div class="container">
						<div class="row">
							<div class="col-md-5">
								<div class="overflow-hidden">
									<span class="top-sub-title text-color-primary d-block appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="100">umroh bersama MawaddahTour</span>{{ $obj->responseMessage }}
								</div>
								<div class="overflow-hidden mb-3">
									<h2 class="word-rotator letters type font-weight-bold text-5 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">
										<span>Solusi untuk</span> 
										<span class="word-rotator-words waiting">
											<b class="is-visible">Paket Murah</b>
											<b>Pelayanan Terbaik</b>
										</span>
									</h2>
								</div>
								<div class="overflow-hidden mb-3">
									<p class="lead mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="400">PT MAWADDAH BERKAH MULIA merupakan jasa penyedia layanan travel umroh dan memiliki izin resmi kementrian agama.</p>
								</div>
								
								<a href="about-us-3.html" class="btn btn-outline btn-rounded btn-primary btn-v-3 btn-h-4 font-weight-bold mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">LIHAT LEBIH LANJUT</a>
							</div>
							<div class="col-md-7">
								<img src="frontend/img/others/desktop-concept-1.png" class="img-fluid concept-pos-1" alt="" />
							</div>
						</div>
					</div>
				</section>
				<section class="section bg-light-5">
					<div class="container">
						<div class="row mb-5">
							<div class="col">
								<div class="overflow-hidden">
									<span class="top-sub-title text-color-primary d-block appear-animation" data-appear-animation="maskUp">4 Alasan</span>
								</div>
								<div class="overflow-hidden mb-3">
									<h2 class="font-weight-bold mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">Mengapa memilih Mawaddah Tour</h2>
								</div>
							</div>
						</div>
						<div class="row align-items-baseline mb-4 pb-2">
							<div class="col-sm-6 col-lg-3">
								<div class="icon-box icon-box-style-3 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="400">
									<div class="icon-box-icon">
										<img width="42" height="42" src="frontend/vendor/linear-icons/tablet.svg" alt="" data-icon data-plugin-options="{'color': '#0fa73e', 'animated': true, 'delay': 400}" />
									</div>
									<div class="icon-box-info">
										<div class="icon-box-info-title">
											<h3 class="font-weight-bold text-4 mb-3">Izin Lengkap</h3>
										</div>
										<p>MawaddahTour memiliki izin resmi kemenag dan legalitasnya sebagai travel umroh No xx Tahun xxxx</p>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3">
								<div class="icon-box icon-box-style-3 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="100">
									<div class="icon-box-icon">
										<img width="48" height="48" src="frontend/vendor/linear-icons/select.svg" alt="" data-icon data-plugin-options="{'color': '#0fa73e', 'animated': true, 'delay': 700}" />
									</div>
									<div class="icon-box-info">
										<div class="icon-box-info-title">
											<h3 class="font-weight-bold text-4 mb-3">Harga Bersaing</h3>
										</div>
										<p>Kami memberikan harga yang murah untuk setiap keberangkatan umroh dengan pelayanan terbaik untuk jamaah</p>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3">
								<div class="icon-box icon-box-style-3 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="100">
									<div class="icon-box-icon">
										<img width="42" height="42" src="frontend/vendor/linear-icons/magnifier.svg" alt="" data-icon data-plugin-options="{'color': '#0fa73e', 'animated': true, 'delay': 1000}" />
									</div>
									<div class="icon-box-info">
										<div class="icon-box-info-title">
											<h3 class="font-weight-bold text-4 mb-3">Berpengalaman</h3>
										</div>
										<p>MawaddahTour telah berdiri sejak xxxx dan telah memberangkatkan jamaah selama xx tahun</p>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3">
								<div class="icon-box icon-box-style-3 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400">
									<div class="icon-box-icon">
										<img width="42" height="42" src="frontend/vendor/linear-icons/tag.svg" alt="" data-icon data-plugin-options="{'color': '#0fa73e', 'animated': true, 'delay': 1300}" />
									</div>
									<div class="icon-box-info">
										<div class="icon-box-info-title">
											<h3 class="font-weight-bold text-4 mb-3">Berangkat Tepat Waktu</h3>
										</div>
										<p>Setiap keberangkatan umroh akan dijadwalkan tepat waktu mulai dari hari, pesawat hingga jam keberangkatan</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section section-skew bg-primary py-5">
					<div class="section-skew-layer section-skew-layer-mobile-right bg-dark-5" data-skew-layer data-skew-layer-value="62%" data-skew-layer-from="right"></div>
					<div class="container">
						<div class="row align-items-center">
							<div class="col-md-9 appear-animation" data-appear-animation="fadeInRightShorter">
								<h2 class="font-weight-semibold text-light mb-0">Informasi lebih lanjut mengenai paket umroh</h2>
								<p class="font-weight-light text-light mb-3 mb-md-0">Hubungi kami melalui email, whatsapp, telp</p>
							</div>
							<div class="col-md-3 text-left text-md-right appear-animation" data-appear-animation="fadeInLeftShorter">
								<a href="#" target="_blank" class="btn btn-light btn-rounded btn-icon-effect-1 font-weight-bold btn-h-4 btn-v-4">
									<span class="wrap">
										<span>Hubungi Kami</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</section>
			
				<section class="section bg-light-5 py-0">
					<div class="container-fluid">
						<div class="row align-items-center">
							<div class="col-md-7 order-2 order-md-1">
								<div class="row">
									<div class="col-sm-6 col-md-4 p-0 appear-animation" data-appear-animation="fadeInScale" data-appear-animation-delay="500">
										<a href="frontend/img/projects/photos/project-1-square.jpg" class="gallery1">
											<span class="image-frame image-frame-style-1 image-frame-effect-1">
												<span class="image-frame-wrapper">
													<img src="frontend/img/projects/generic/project-1-square.jpg" class="img-fluid" alt="1">
													<span class="image-frame-inner-border"></span>
													<span class="image-frame-action">
														<span class="image-frame-action-icon">
															<i class="lnr lnr-link text-color-light"></i>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>

									<div class="col-sm-6 col-md-4 p-0 appear-animation" data-appear-animation="fadeInScale" data-appear-animation-delay="500">
										<a href="frontend/img/projects/photos/project-2-square.jpg" class="gallery1">
											<span class="image-frame image-frame-style-1 image-frame-effect-1">
												<span class="image-frame-wrapper">
													<img src="frontend/img/projects/photos/project-2-square.jpg" class="img-fluid" alt="2">
													<span class="image-frame-inner-border"></span>
													<span class="image-frame-action">
														<span class="image-frame-action-icon">
															<i class="lnr lnr-link text-color-light"></i>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>

									<div class="col-sm-6 col-md-4 p-0 appear-animation" data-appear-animation="fadeInScale" data-appear-animation-delay="500">
										<a href="frontend/img/projects/photos/project-3-square.jpg" class="gallery1">
											<span class="image-frame image-frame-style-1 image-frame-effect-1">
												<span class="image-frame-wrapper">
													<img src="frontend/img/projects/generic/project-3-square.jpg" class="img-fluid" alt="3">
													<span class="image-frame-inner-border"></span>
													<span class="image-frame-action">
														<span class="image-frame-action-icon">
															<i class="lnr lnr-link text-color-light"></i>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>

									<div class="col-sm-6 col-md-4 p-0 appear-animation" data-appear-animation="fadeInScale" data-appear-animation-delay="500">
										<a href="frontend/img/projects/photos/project-3-square.jpg" class="gallery1">
											<span class="image-frame image-frame-style-1 image-frame-effect-1">
												<span class="image-frame-wrapper">
													<img src="frontend/img/projects/photos/project-3-square.jpg" class="img-fluid" alt="4">
													<span class="image-frame-inner-border"></span>
													<span class="image-frame-action">
														<span class="image-frame-action-icon">
															<i class="lnr lnr-link text-color-light"></i>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>

									<div class="col-sm-6 col-md-4 p-0 appear-animation" data-appear-animation="fadeInScale" data-appear-animation-delay="500">
										<a href="frontend/img/projects/photos/project-4-square.jpg" class="gallery1">
											<span class="image-frame image-frame-style-1 image-frame-effect-1">
												<span class="image-frame-wrapper">
													<img src="frontend/img/projects/generic/project-4-square.jpg" class="img-fluid" alt="5">
													<span class="image-frame-inner-border"></span>
													<span class="image-frame-action">
														<span class="image-frame-action-icon">
															<i class="lnr lnr-link text-color-light"></i>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>

									<div class="col-sm-6 col-md-4 p-0 appear-animation" data-appear-animation="fadeInScale" data-appear-animation-delay="500">
										<a href="frontend/img/projects/photos/project-6-square.jpg" class="gallery1">
											<span class="image-frame image-frame-style-1 image-frame-effect-1">
												<span class="image-frame-wrapper">
													<img src="frontend/img/projects/photos/project-6-square.jpg" class="img-fluid" alt="6">
													<span class="image-frame-inner-border"></span>
													<span class="image-frame-action">
														<span class="image-frame-action-icon">
															<i class="lnr lnr-link text-color-light"></i>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
							</div>
							<div class="col-md-5 text-center order-1 order-md-2 py-5 py-md-0 px-xl-5">
								<h2 class="font-weight-bold mb-3 mb-lg-5 px-lg-5 mx-xl-5">Galeri keberangkatan<br>bersama MawaddahTour</h2>
								<div class="d-block d-sm-inline-block text-center" >
									<a href="portfolio-grid-4-columns.html" class="btn btn-outline btn-rounded btn-primary btn-v-3 btn-h-4 font-weight-bold">LIHAT GALERI</a>
									<span class="d-block mt-3">atau<a class="text-underline ml-2" href="contact-us-5.html">HUBUNGI KAMI</a></span>
								</div>
							</div>
						</div>

					</div>
				</section>
				
@endsection

@section('js')
<script>
	window.onload = function() {
		halkaBox.run("gallery1");
	};

</script>
@endsection