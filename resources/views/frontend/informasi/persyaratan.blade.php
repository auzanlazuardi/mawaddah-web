@extends('frontend.master.layout')

@section('content')
<section class="page-header">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-md-8 text-left">
								<span class="tob-sub-title text-color-primary d-block">Informasi</span>
								<h1 class="font-weight-bold">Perysaratan Umroh</h1>
								<p class="lead">Syarat dan ketentuan pendaftaran umroh beserta harga yang sudah termasuk maupun belum termasuk dalam setiap paket</p>
							</div>
							<div class="col-md-4">
								<ul class="breadcrumb justify-content-start justify-content-md-end mb-0">
									<li><a href="{{ route('beranda') }}">Beranda</a></li>
									<li>Informasi</li>
									<li class="active">Persyaratan</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				
				<div class="container">
					<div class="row">
						<aside class="sidebar col-md-4 col-lg-3 order-2 order-md-1">
							<div class="accordion accordion-default accordion-toggle accordion-style-1" data-plugin-sticky data-plugin-options="{'offset_top': 100}" role="tablist">

								<div class="card">
									<div class="card-header accordion-header" role="tab" id="pages">
										<h3 class="text-3 mb-0">
											<a href="#" data-toggle="collapse" data-target="#togglePages" aria-expanded="false" aria-controls="togglePages">CONTENTS</a>
										</h3>
									</div>
									<div id="togglePages" class="accordion-body collapse show" aria-labelledby="pages">
										<div class="card-body">
											<ul class="list list-unstyled">
												<li class="mb-2">
													<a data-hash data-hash-offset="100" href="#syarat-pendaftaran" class="font-weight-semibold"><i class="fas fa-angle-right ml-1 mr-1 pr-2"></i> Syarat Pendaftaran</a>
												</li>
												<li class="mb-2">
													<a data-hash data-hash-offset="100" href="#harga-sudah-termasuk" class="font-weight-semibold"><i class="fas fa-angle-right ml-1 mr-1 pr-2"></i> Harga Sudah Termasuk</a>
												</li>
												<li class="mb-2">
													<a data-hash data-hash-offset="100" href="#harga-belum-termasuk" class="font-weight-semibold"><i class="fas fa-angle-right ml-1 mr-1 pr-2"></i> Harga Belum Termasuk</a>
												</li>
												<li class="mb-2">
													<a data-hash data-hash-offset="100" href="#pembatalan" class="font-weight-semibold"><i class="fas fa-angle-right ml-1 mr-1 pr-2"></i> Pembatalan</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								
							</div>
						</aside>
						<div class="col-md-8 col-lg-9 order-1 order-md-2 mb-5 mb-md-0">
							<div class="row">
								<div class="col">
									
									<div id="syarat-pendaftaran"" class="row mb-5">
										<div class="col">
											<h2 class="font-weight-bold text-4 mb-3">Syarat Pendaftaran</h2>
											<ul class="list list-unstyled font-weight-semibold text-2 mb-0">
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Paspor Asli dengan masa berlaku tidak kurang dari 10 Bulan sebelum Jadwal Keberangkatan</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Nama dipaspor minimal 3 (tiga) kata, contoh : Muhammad Ali Akbar</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Pas Foto 4X 6 Berwarna background Putih dengan focus wajah 80% sebanyak 6 (enam) lembar</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Fotokopi KTP</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Fotokopi Kartu Keluaarga (KK) dan Buku Nikah bagi Suami Istri</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Minimal melakukan DP sebesar Rp 5.000.000,-</a></li>
											</ul>
										</div>
									</div>
									<div id="harga-sudah-termasuk" class="row mb-5">
										<div class="col">
											<h2 class="font-weight-bold text-4 mb-3">Harga Sudah Termasuk</h2>
											<ul class="list list-unstyled font-weight-semibold text-2 mb-0">
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What does royalty free mean?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What do you mean by item and end product?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What are some examples of permitted end products?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Am I allowed to modify the item that I purchased?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What does non-exclusive mean?</a></li>
											</ul>
										</div>
									</div>
									<div id="harga-belum-termasuk" class="row mb-5">
										<div class="col">
											<h2 class="font-weight-bold text-4 mb-3">Harga Belum Termasuk</h2>
											<ul class="list list-unstyled font-weight-semibold text-2 mb-0">
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What does royalty free mean?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What do you mean by item and end product?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What are some examples of permitted end products?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Am I allowed to modify the item that I purchased?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What does non-exclusive mean?</a></li>
											</ul>
										</div>
									</div>
									<div id="pembatalan" class="row">
										<div class="col">
											<h2 class="font-weight-bold text-4 mb-3">Pembatalan</h2>
											<ul class="list list-unstyled font-weight-semibold text-2 mb-0">
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What does royalty free mean?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What do you mean by item and end product?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What are some examples of permitted end products?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">Am I allowed to modify the item that I purchased?</a></li>
												<li class="mb-2"><a data-hash data-hash-highlight data-hash-offset="100">What does non-exclusive mean?</a></li>
											</ul>
										</div>
									</div>
									
									<hr class="my-5">
									<div id="faq-1-1" class="row mb-5">
										<div class="col">
											<h3 class="font-secondary font-weight-bold text-2 mb-3">What does royalty free mean?</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur malesuada malesuada metus ut placerat. Cras a porttitor quam, eget ornare sapien. In sit amet vulputate metus. Nullam eget rutrum nisl. Sed tincidunt lorem sed maximus interdum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean scelerisque efficitur mauris nec tincidunt. Ut cursus leo mi, eu ultricies magna faucibus id.</p>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
@endsection