<footer id="footer" class="footer-hover-links-light mt-0">
				<div class="container">
					
					<div class="row">
						<div class="col-lg-4 mb-4 mb-md-0">
							<a href="index.html" class="logo">
								<img alt="EZY Website Template" class="img-fluid mb-3" src="frontend/img/logo-footer.png">
							</a>
							<p>PT MAWADDAH BERKAH MULIA merupakan jasa penyedia layanan travel umroh dan memiliki izin resmi kementrian agama.</p>
						</div>
						<div class="col-lg-4 ml-auto mt-lg-4 pt-lg-2">
							<ul class="list list-icon list-unstyled mb-0 mb-lg-3">
								<li class="mb-2"><i class="fas fa-angle-right mr-2 ml-1"></i> <span class="text-color-light">Alamat:</span> 411 Mart Lt.2 Kalidoni, Palembang. Indonesia</li>
								<li class="mb-2"><i class="fas fa-angle-right mr-2 ml-1"></i> <span class="text-color-light">Phone:</span> <a href="tel:+1234567890">(+62) 81373999966</a></li>
								<li class="mb-2"><i class="fas fa-angle-right mr-2 ml-1"></i> <span class="text-color-light">Email:</span> <a href="mailto:support@mawaddahtour.com" class="link-underline-light">mail@example.com</a></li>
							</ul>
						</div>
						<div class="col-lg-3 mt-lg-4 pt-lg-2">
							<ul class="list list-icon list-unstyled">
								<li class="mb-2"><i class="fas fa-angle-right mr-2 ml-1"></i> <a href="https://themeforest.net/item/ezy-responsive-multipurpose-html5-template/21814871">Persyaratan Umroh</a></li>
								<li class="mb-2"><i class="fas fa-angle-right mr-2 ml-1"></i> <a href="contact-us-2.html">Tentang Kami</a></li>
								<li class="mb-2"><i class="fas fa-angle-right mr-2 ml-1"></i> <a href="contact-us-3.html">Hubungi Kami</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row text-center text-md-left align-items-center">
							<div class="col-md-10 col-lg-10">
								<p class="text-md-left pb-0 mb-0">Copyrights © 2018. All Rights Reserved by PT MAWADDAH BERKAH MULIA</p>
							</div>
							<div class="col-md-2 col-lg-2">
								<ul class="social-icons social-icons-transparent social-icons-icon-light social-icons-lg">
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
									<li class="social-icons-instagram"><a href="http://www.instagram.com/mawaddahtour" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
								</ul>
							</div>
							
						</div>
					</div>
				</div>
			</footer>