<header id="header" class="{{ Request::path() == '/' ? 'header-transparent' : '' }} header-with-borders header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickyHeaderContainerHeight': 72, 'stickyChangeLogo': true}">
				<div class="header-body">
					<div class="header-container">
						<div class="header-row">
							<div class="header-column justify-content-start">
								<div class="header-logo border-left-0 px-4">
									<a href="index.html">
										<img alt="EZ" width="75" height="50" data-change-src="{{ asset('frontend/img/logo-small.png')}}" src="{{ asset('frontend/img/logo-small-light.png')}}">
									</a>
								</div>
								<div class="header-nav justify-content-lg-start ml-3">
									<div class="header-nav-main header-nav-main-uppercase header-nav-main-effect-1 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav flex-column flex-lg-row" id="mainNav">
												<li class="dropdown">
													<a class="{{ Request::is('/') ? 'active' : '' }}" href="{{ route('beranda') }}">
														Beranda
													</a>
												</li>
												<li class="dropdown">
													<a class="{{ Request::is('galeri') ? 'active' : '' }}" href="{{ route('galeri') }}">
														Galeri
													</a>
												</li>
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle {{ Request::is('informasi/*') ? 'active' : '' }}" href="#">
														Informasi
													</a>
													<ul class="dropdown-menu">
														<li class="dropdown-submenu">
															<a class="dropdown-item dropdown-toggle" href="{{ route('tentang-kami') }}">Tentang Kami</a>
														</li>
														<li class="dropdown-submenu">
															<a class="dropdown-item dropdown-toggle" href="{{ route('persyaratan') }}">Persyaratan Umroh</a>
														</li>
														
													</ul>
												</li>
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle {{ Request::is('kontak') ? 'active' : '' }}" href="#">
														Bantuan
													</a>
													<ul class="dropdown-menu">
														<li class="dropdown-submenu">
															<a class="dropdown-item dropdown-toggle" href="{{ route('kontak') }}">Hubungi Kami</a>
														</li>
													</ul>
												</li>
											</ul>
										</nav>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<ul class="header-social-icons social-icons border-right-0 d-none d-xl-flex px-4">
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
									<li class="social-icons-instagram"><a href="http://www.instagram.com/mawaddahtour" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
								</ul>
								<button class="header-btn-collapse-nav header-btn-collapse-nav-light on-sticky-dark mx-3" data-toggle="collapse" data-target=".header-nav-main nav">
									<span class="hamburguer">
										<span></span>
										<span></span>
										<span></span>
									</span>
									<span class="close">
										<span></span>
										<span></span>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</header>