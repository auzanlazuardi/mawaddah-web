<!DOCTYPE html>
<html lang="zxx">
	<head>

		@include('frontend.master.head')

	</head>
	<body>
		<div class="body">

			@include('frontend.master.header')

			<div role="main" class="main">
				@yield('content')
 			</div>
			
			@include('frontend.master.footer')
		</div>

		@include('frontend.master.foot')

	</body>
</html>
