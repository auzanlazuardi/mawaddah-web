<?php

Route::get('/', 'Frontend\ViewController@index')->name('beranda');
Route::get('galeri', 'Frontend\ViewController@galeri')->name('galeri');
Route::get('informasi/tentang-kami', 'Frontend\ViewController@tentangkami')->name('tentang-kami');
Route::get('informasi/persyaratan', 'Frontend\ViewController@persyaratan')->name('persyaratan');
Route::get('kontak', 'Frontend\ViewController@kontak')->name('kontak');

//SEND EMAIL
Route::post('send/email', 'Frontend\ViewController@sendEmail')->name('sendEmail');

